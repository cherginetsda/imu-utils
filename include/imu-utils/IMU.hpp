#ifndef IMU_UTILS_IMU_HPP_
#define IMU_UTILS_IMU_HPP_

#include <iostream>
#include <string>
#include <Eigen/Core>
#include <yaml-cpp/yaml.h>
#include <yaml-cpp/parser.h>

namespace imu 
{

    class IMU // TODO: Сделать отдельный репозиторий со всем необходимым для IMU
    {
        private:
            YAML::Node config;
            bool is_new_data_accelerometer;
            bool is_new_data_gyroscope;

            Eigen::Vector3d data_accelerometer;
            Eigen::Vector3d data_gyroscope;

            Eigen::Vector3d bias_accelerometer;
            Eigen::Vector3d bias_gyroscope;

            Eigen::Matrix3d rot_scale_matrix_accelerometer;            
            Eigen::Matrix3d rot_scale_matrix_gyroscope;     

        public:
            IMU(std::string config_filename);
            

            Eigen::Vector3d updateAcceleration(Eigen::Vector3d data);
            Eigen::Vector3d getAcceleration();
            bool isAccelerationDataNew();

            Eigen::Vector3d updateAngularVelocity(Eigen::Vector3d data);
            Eigen::Vector3d getAngularVelocity();
            bool isAngularVelocityDataNew();

    };

}

#endif // IMU_UTILS_IMU_HPP_