#include "imu-utils/IMU.hpp"

imu::IMU::IMU(std::string config_filename)
{
    std::cout << "IMU config: " << config_filename << std::endl;
    config = YAML::LoadFile(config_filename);
    YAML::Node  acc_param = config["acc_corr_params"];
    YAML::Node gyro_param = config["gyro_corr_params"];

    Eigen::Matrix3d scale_matrix_accelerometer;            
    Eigen::Matrix3d scale_matrix_gyroscope;

    Eigen::Matrix3d rot_matrix_accelerometer;            
    Eigen::Matrix3d rot_matrix_gyroscope;

    if (acc_param)
    {
        rot_matrix_accelerometer <<     1, -acc_param[0].as<double>(), acc_param[1].as<double>(),
                                        0, 1, -acc_param[2].as<double>(),
                                        0, 0, 1;
        scale_matrix_accelerometer <<   acc_param[3].as<double>(), 0, 0,
                                        0, acc_param[4].as<double>(), 0,
                                        0, 0, acc_param[5].as<double>();
        bias_accelerometer <<   acc_param[6].as<double>(), 
                                acc_param[7].as<double>(), 
                                acc_param[8].as<double>();
        rot_scale_matrix_accelerometer = rot_matrix_accelerometer * scale_matrix_accelerometer;
    }
    else
    {
        rot_scale_matrix_accelerometer <<  Eigen::MatrixXd::Identity(3,3);
        bias_accelerometer << Eigen::MatrixXd::Zero(3,1);
    }
    if (gyro_param)
    {
        rot_matrix_gyroscope <<     1, -gyro_param[0].as<double>(), gyro_param[1].as<double>(),
                                    gyro_param[3].as<double>(), 1, -gyro_param[2].as<double>(),
                                    -gyro_param[4].as<double>(), gyro_param[5].as<double>(), 1;

        scale_matrix_gyroscope <<   gyro_param[6].as<double>(), 0, 0,
                                    0, gyro_param[7].as<double>(), 0,
                                    0, 0, gyro_param[8].as<double>();
        bias_gyroscope <<   gyro_param[9].as<double>(), 
                            gyro_param[10].as<double>(), 
                            gyro_param[11].as<double>();
        rot_scale_matrix_gyroscope = rot_matrix_gyroscope * scale_matrix_gyroscope;
    }
    else
    {
        rot_scale_matrix_gyroscope <<  Eigen::MatrixXd::Identity(3,3);
        bias_gyroscope << Eigen::MatrixXd::Zero(3,1);
    }
    is_new_data_accelerometer = false;
    is_new_data_gyroscope = false;
}

Eigen::Vector3d imu::IMU::updateAcceleration(Eigen::Vector3d data)
{
    is_new_data_accelerometer = true;
    data_accelerometer = rot_scale_matrix_accelerometer * (data +  bias_accelerometer);
    return data_accelerometer;
}

bool imu::IMU::isAccelerationDataNew()
{
    return is_new_data_accelerometer;
}

Eigen::Vector3d imu::IMU::getAcceleration()
{
    is_new_data_accelerometer = false;
    return data_accelerometer;
}

Eigen::Vector3d imu::IMU::updateAngularVelocity(Eigen::Vector3d data)
{
    is_new_data_gyroscope = true;
    data_gyroscope = rot_scale_matrix_gyroscope * (data +  bias_gyroscope);
    return data_gyroscope;
}

bool imu::IMU::isAngularVelocityDataNew()
{
    return is_new_data_gyroscope;
}

Eigen::Vector3d imu::IMU::getAngularVelocity()
{
    is_new_data_gyroscope = false;
    return data_gyroscope;
}